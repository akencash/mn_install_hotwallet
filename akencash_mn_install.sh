#!/bin/bash
#
# Copyright (C) 2018 AKN Team <dev@aken.cash>
#
# mn_install.sh is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# mn_install.sh is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with mn_install.sh. If not, see <http://www.gnu.org/licenses/>
#

# Only Ubuntu 16.04 supported at this moment.

set -o errexit

# OS_VERSION_ID=`gawk -F= '/^VERSION_ID/{print $2}' /etc/os-release | tr -d '"'`

if [[ "$1" == "install" ]]; then
    sudo apt-get update && sudo apt-get upgrade -y
    sudo apt install wget git python3 python3-pip python-virtualenv -y

    AKN_DAEMON_USER_PASS=`head /dev/urandom | tr -dc A-Za-z0-9 | head -c 24 ; echo ""`
    AKN_DAEMON_RPC_PASS=`head /dev/urandom | tr -dc A-Za-z0-9 | head -c 24 ; echo ""`
    MN_EXTERNAL_IP=`curl -s ifconfig.co`

    sudo useradd -U -m akencash -s /bin/bash
    echo "akencash:${AKN_DAEMON_USER_PASS}" | sudo chpasswd
    sudo wget https://github.com/akencash/akencash/releases/download/v0.4.3.1/akencash-cli-linux-x64.tar.gz --directory-prefix /home/akencash/
    sudo tar -xzvf /home/akencash/akencash-cli-linux-x64.tar.gz -C /home/akencash/
    sudo rm /home/akencash/akencash-cli-linux-x64.tar.gz
    sudo mkdir /home/akencash/.akencashcore/
    sudo chown -R akencash:akencash /home/akencash/akencash*
    sudo chmod 755 /home/akencash/akencash*
    echo -e "rpcuser=akenrpc\nrpcpassword=${AKN_DAEMON_RPC_PASS}\nlisten=1\nserver=1\nrpcallowip=127.0.0.1\nmaxconnections=256" | sudo tee /home/akencash/.akencashcore/akencash.conf
    sudo chown -R akencash:akencash /home/akencash/.akencashcore/
    sudo chown 500 /home/akencash/.akencashcore/akencash.conf

    sudo tee /etc/systemd/system/akencash.service <<EOF
[Unit]
Description=Akencash, distributed currency daemon
After=network.target

[Service]
User=akencash
Group=akencash
WorkingDirectory=/home/akencash/
ExecStart=/home/akencash/akencashd

Restart=always
PrivateTmp=true
TimeoutStopSec=60s
TimeoutStartSec=2s
StartLimitInterval=120s
StartLimitBurst=5

[Install]
WantedBy=multi-user.target
EOF

    sudo systemctl enable akencash
    sudo systemctl start akencash
    echo "Waiting for AKN node boot and keypool being generated"
    sleep 14

    MN_ADDR_PUBKEY=`sudo -H -u akencash /home/akencash/akencash-cli getnewaddress`
    MN_ADDR_PRIVKEY=`sudo -H -u akencash /home/akencash/akencash-cli dumpprivkey ${MN_ADDR_PUBKEY}`

    MNGENKEY=`sudo -H -u akencash /home/akencash/akencash-cli masternode genkey`
    echo -e "masternode=1\nmasternodeprivkey=${MNGENKEY}\nexternalip=${MN_EXTERNAL_IP}:14933" | sudo tee -a /home/akencash/.akencashcore/akencash.conf

    echo "Installing sentinel engine"
    sudo git clone https://github.com/akencash/sentinel.git /home/akencash/sentinel/
    sudo chown -R akencash:akencash /home/akencash/sentinel/
    cd /home/akencash/sentinel/
    sudo -H -u akencash virtualenv -p python3 ./venv
    sudo -H -u akencash ./venv/bin/pip install -r requirements.txt
    echo "* * * * * akencash cd /home/akencash/sentinel && ./venv/bin/python bin/sentinel.py >/dev/null 2>&1" | sudo tee /etc/cron.d/akencash_sentinel
    sudo chmod 644 /etc/cron.d/akencash_sentinel

    echo " "
    echo " "
    echo "==============================="
    echo "Masternode installed!"
    echo "==============================="
    echo "Copy and keep that information in secret to avoid coins loss:"
    echo "Masternode key: ${MNGENKEY}"
    echo "Masternode addr pubkey: ${MN_ADDR_PUBKEY}"
    echo "Masternode addr privkey: ${MN_ADDR_PRIVKEY}"
    echo " "
    echo "Next step: activate"
    echo "Activation requires 3000 AKN on address: ${MN_ADDR_PUBKEY}"
    echo "Send coins, wait for 15 confirmations and then run: ./akencash_mn_install.sh activate"

    exit 0

elif [[ "$1" == "activate" ]]; then

    echo "Beginning masternode activation"
    sudo systemctl restart akencash
    echo "Waiting for masternode network meta syncing..."
    sleep 90

    MN_EXTERNAL_IP=`curl -s ifconfig.co`
    MNKEY=`sudo cat /home/akencash/.akencashcore/akencash.conf | grep masternodeprivkey | cut -d "=" -f2`
    MN_INPUT_TX=`sudo -H -u akencash /home/akencash/akencash-cli masternode outputs | sed -n "2p" | cut -d \" -f2`
    MN_INPUT_INDEX=`sudo -H -u akencash /home/akencash/akencash-cli masternode outputs | sed -n "2p" | cut -d \" -f4`

    if [[ "$MN_INPUT_TX" == "}" ]]; then
        echo "No masternode outputs found. Make sure you've sent coins on masternode's address and waited for 15 confirmations."
        exit 1
    fi
    echo -e "mn1 ${MN_EXTERNAL_IP}:14933 ${MNKEY} ${MN_INPUT_TX} ${MN_INPUT_INDEX}" | sudo tee -a /home/akencash/.akencashcore/masternode.conf
    sudo chown akencash:akencash /home/akencash/.akencashcore/masternode.conf

    echo "Prepared conf files, restarting AKN node"

    sudo systemctl restart akencash
    echo "Waiting for masternode network meta syncing..."
    sleep 90

    echo "Announcing new masternode in AKN network"
    sudo -H -u akencash /home/akencash/akencash-cli masternode start-alias mn1
    sudo -H -u akencash /home/akencash/akencash-cli masternode list-conf

    echo "==============================="
    echo "Masternode activated!"
    echo "==============================="
    echo "After ~10-20 min it would change status to \"ENABLED\"."

    exit 0


else
    echo "Choose action: install or activate (ex.: ./akencash_mn_install.sh install)"
    echo "install - initial prepare node before sending coins"
    echo "activate - must be used after sending coins with at least 15 confirmations"
    exit 1
fi
